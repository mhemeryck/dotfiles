# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

zstyle ':omz:update' mode reminder  # just remind me to update when it's time

ZSH_THEME="simple"

plugins=(
  git
  direnv
  common-aliases
  kubectl
  poetry
  gpg-agent
  # rye
  gcloud
)

source $ZSH/oh-my-zsh.sh

# helix everywhere
export EDITOR=hx

# Terminal vi mode
set -o vi

# Local bin
[[ -d $HOME/.local/bin ]] && export PATH=$HOME/.local/bin:$PATH

# golang
[[ -d $HOME/Projects/go ]] && export GOPATH=$HOME/Projects/go
[[ -d $GOPATH/bin ]] && PATH=$PATH:$GOPATH/bin

# yarn
[[ -f /usr/bin/yarn ]] && export PATH="$(yarn global bin):$PATH"

# cargo
[[ -d $HOME/.cargo/bin ]] && export PATH=$PATH:$HOME/.cargo/bin

# add Pulumi to the PATH
[[ -d $HOME/.pulumi/bin ]] && export PATH=$PATH:$HOME/.pulumi/bin

# rye
# [[ -f $HOME/.rye/env ]] && source "$HOME/.rye/env"

# bun
export BUN_INSTALL="$HOME/.bun"
if [ -d "$BUN_INSTALL"  ]
  then
  # bun completions
  [ -s "/home/mhemeryck/.bun/_bun" ] && source "/home/mhemeryck/.bun/_bun"
  export PATH="$BUN_INSTALL/bin:$PATH"
fi

# JAVA: sdkman
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# fzf fuzzy finder -- after vi mode
[[ -d /usr/share/fzf ]] && source /usr/share/fzf/shell/key-bindings.zsh

autoload -Uz compinit
zstyle ':completion:*' menu select
fpath+=~/.zfunc
