" remove vi compatibility
set nocompatible

" plugin manager
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


call plug#begin('~/.vim/plugged')
    " fuzzy file searcher
    Plug 'junegunn/fzf.vim'
    " distraction-free writing
    Plug 'junegunn/goyo.vim', { 'for': 'markdown' }
    " hyperfocused writing
    Plug 'junegunn/limelight.vim', { 'for': 'markdown' }
    " easy alignment
    Plug 'junegunn/vim-easy-align'
    " tender color scheme
    Plug 'jacoborus/tender.vim'
    " monokai color scheme
    Plug 'sickill/vim-monokai'
    " tree explorer
    Plug 'preservim/nerdtree'
    " status line
    Plug 'vim-airline/vim-airline'
    " git integration
    Plug 'tpope/vim-fugitive'
    " surround
    Plug 'tpope/vim-surround'
    " python autocomplete
    Plug 'davidhalter/jedi-vim', { 'for': 'python' }
    " async linters and fixers
    Plug 'dense-analysis/ale'
    " ctags support
    Plug 'ludovicchabant/vim-gutentags'
    " terraform
    Plug 'hashivim/vim-terraform'
    " terraform lsp
    Plug 'prabirshrestha/vim-lsp'
    " elm
    Plug 'elm-tooling/elm-vim'
    " golang
    Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
    "" zig
    "Plug 'ziglang/zig.vim'
    "" Autocomplete
    "Plug 'ycm-core/YouCompleteMe'
    " rust
    Plug 'rust-lang/rust.vim'
call plug#end()
filetype plugin indent on

" general
syntax on

" swap file in dedicated folder instead of local
set directory=~/.vim/swp//
set backupdir=~/.vim/backup//

" colors
silent! colors tender
set background=dark

" mac-specific check for backspace
if has('macunix')
    set backspace=indent,eol,start
endif

" split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" customize fzf colors to match your color scheme
let g:fzf_colors =
    \ { 'fg':    ['fg', 'Normal'],
    \ 'bg':      ['bg', 'Normal'],
    \ 'hl':      ['fg', 'Comment'],
    \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
    \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
    \ 'hl+':     ['fg', 'Statement'],
    \ 'info':    ['fg', 'PreProc'],
    \ 'border':  ['fg', 'Ignore'],
    \ 'prompt':  ['fg', 'Conditional'],
    \ 'pointer': ['fg', 'Exception'],
    \ 'marker':  ['fg', 'Keyword'],
    \ 'spinner': ['fg', 'Label'],
    \ 'header':  ['fg', 'Comment'] }

" ripgrep-based searching
if executable('rg')
     command! -bang -nargs=* Rg
     \ call fzf#vim#grep(
     \   'rg --column --line-number --no-heading --color=always --smart-case -- '.shellescape(<q-args>), 1,
     \   fzf#vim#with_preview(), <bang>0)
endif

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" default indent behavior
au FileType python,javascript,vue,sh,markdown.pandoc,hcl,go set tabstop=4 shiftwidth=4 expandtab

" git commit message
au FileType gitcommit set textwidth=72

" yaml, html
au FileType html,htmldjango,yaml set shiftwidth=2 tabstop=2 expandtab

" markdown
au BufNewFile,BufRead *.md set filetype=markdown.pandoc

nmap <leader>e :NERDTree<CR>
let NERDTreeIgnore=['\.pyc$', '__pycache__']

" airline: font fixes etc.
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0

" tab-like behavior with buffers
nmap <leader>q :bd<CR>

" removing trailing spaces
nmap <leader><SPACE> :%s/\s\+$//e<CR>

" fzf mappings
nmap <leader><leader> :Buffers<CR>
nmap <leader>f :Files<CR>
nmap <leader>c :Commits<CR>
nmap <leader>s :Rg<CR>
nmap <leader>t :Tags<CR>

" python
autocmd FileType python nmap <leader>pdb obreakpoint()<ESC>

let g:ale_completion_enabled = 1
" ale fixers
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\   'python': ['black', 'isort'],
\   'yaml': ['prettier'],
\   'markdown': ['prettier'],
\   'json': ['prettier'],
\   'elm': ['elm-format'],
\   'rust': ['rustfmt'],
\}
let g:ale_linters = {
\  'rust': ['analyzer'],
\}
let g:airline#extensions#ale#enabled = 1

" autoformat
nmap <silent><leader>aj :ALENext<CR>
nmap <silent><leader>ak :ALEPrevious<CR>
nmap <silent><leader>af :ALEFix<CR>

"" zig ycm
"let g:ycm_filetype_whitelist = {'zls': 1}
"let g:ycm_language_server =
"\ [
"\{
"\     'name': 'zls',
"\     'filetypes': [ 'zig' ],
"\     'cmdline': [ 'usr/local/bin/zls' ]
"\    }
"\ ]

let g:jedi#goto_stubs_command="<leader>x"

" LSP setup for terraform
augroup lsp
  autocmd!
  autocmd FileType terraform setlocal omnifunc=v:lua.vim.lsp.omnifunc
  autocmd FileType terraform setlocal lsp=terraform_ls
augroup end
